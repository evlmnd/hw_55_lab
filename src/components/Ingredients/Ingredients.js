import SaladImage from '../../assets/salad.svg';
import CheeseImage from '../../assets/cheese.svg';
import MeatImage from '../../assets/meat.svg';
import BaconImage from '../../assets/bacon.svg';

const Ingredients = () => {
    const INGREDIENTS = [
        {name: 'Salad', price: 5, image: SaladImage},
        {name: 'Cheese', price: 20, image: CheeseImage},
        {name: 'Meat', price: 50, image: MeatImage},
        {name: 'Bacon', price: 30, image: BaconImage}
    ];
    return INGREDIENTS;
};


export default Ingredients;