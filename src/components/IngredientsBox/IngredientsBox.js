import React from 'react';

import './IngredientsBox.css';

const IngredientsBox = props => (
    <div className="Ingredients-box">
        <span className="box-name">Ingredients</span>
        <ul>
            {props.ingredients.map((ingredient, index) => {
                return <li key={index}>
                    <img src={ingredient.image} alt={"ingredient"}/>
                    <span className="ingredient-name">{ingredient.name}</span>
                    <span>x{props.burgerIngredients.find(x => x.name === ingredient.name).count}</span>
                    <span className="ingredient-price">{'$' + ingredient.price}</span>
                    <button type="button" onClick={props.clickAdd} id={ingredient.name}>Add</button>
                    <button type="button" onClick={props.clickRemove} id={ingredient.name + '-remove'}>x</button>
                </li>
            })}
        </ul>

    </div>
);

export default IngredientsBox;