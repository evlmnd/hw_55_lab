import React from 'react';

import './Burger.css';

const Burger = props => (

    <div className="Burger-box">
        <div className="Burger">
            <span className="box-name">Burger</span>
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {props.ingr}

            <div className="BreadBottom"></div>
        </div>

    </div>
);

export default Burger;
