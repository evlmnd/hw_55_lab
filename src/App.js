import React, {Component} from 'react';
import IngredientsBox from './components/IngredientsBox/IngredientsBox';
import Ingredients from './components/Ingredients/Ingredients';
import Burger from './components/Burger/Burger';

import './App.css';


class App extends Component {
    state = {
        ingredients: [
            {name: 'Salad', count: 0},
            {name: 'Cheese', count: 0},
            {name: 'Meat', count: 0},
            {name: 'Bacon', count: 0}
        ],
        sum: 0
    };

    addIngredient = event => {
        const ingredients = this.state.ingredients;
        let sum = this.state.sum;

        const ingredient = ingredients.find(x => x.name === event.target.id);
        ingredient.count++;
        const price = (new Ingredients()).find(x => x.name === event.target.id).price;
        sum+=price;

        this.setState({ingredients, sum});
    };

    removeIngredient = event => {
        const ingredients = this.state.ingredients;
        let sum = this.state.sum;

        const ingredient = ingredients.find(x => (x.name + '-remove') === event.target.id);
        const price = (new Ingredients()).find(x => x.name + '-remove' === event.target.id).price;
        if (ingredient.count > 0) {
            ingredient.count = ingredient.count - 1;
            sum-=price;
        }
        this.setState({ingredients, sum});
    };


    render() {
        let ingredients = [];

        this.state.ingredients.forEach(function (ingr) {
            if(ingr.count > 0) {
                for(let i = 0; i < ingr.count; i++) {
                    ingredients.push(<div className={ingr.name} key={ingr.name+i}/>);
                }
            }
        });

        return (
            <div className="App">
                <div className="sum">Sum: {'$' + this.state.sum}</div>
                <div className="box">
                    <IngredientsBox ingredients={new Ingredients()}
                                    burgerIngredients={this.state.ingredients}
                                    clickAdd={(event) => this.addIngredient(event)}
                                    clickRemove={(event) => this.removeIngredient(event)}/>
                    <Burger ingredients={this.state.ingredients}  ingr={ingredients}/>
                </div>
            </div>
        );
    }
}

export default App;
